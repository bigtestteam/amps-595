-Dproduct.version=${product.version}
-Dproduct.data.version=${product.data.version}
${invoker.product}:compress-resources process-resources -DskipAllPrompts=true
